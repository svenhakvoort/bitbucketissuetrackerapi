<?php

namespace BitBucketApi;




class BitbucketIssueTrackerApi {

    private $apiKey;
    private $consumerKey;
    private $consumerSecret;
    private $refreshToken;
    private $configFile = __DIR__.'/config.ini';
    private $repoSuffix;

    function __construct()
    {
        $ini = parse_ini_file($this->configFile);

        if (isset($ini["refresh_token"])) {
            $this->refreshToken = $ini["refresh_token"];
            $this->consumerKey = $ini["consumer_key"];
            $this->consumerSecret = $ini["consumer_secret"];
            $this->repoSuffix = $ini["repo"];
            $this->apiKey = $this->_getApiKey();
        }
    }

    function __autoload($class)
    {
        $parts = explode('\\', $class);
        require end($parts) . '.php';
    }

    private function _getApiKey() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bitbucket.org/site/oauth2/access_token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=".$this->refreshToken,
            CURLOPT_USERPWD => "$this->consumerKey:$this->consumerSecret",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = json_decode(curl_exec($curl), TRUE);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return $response['access_token'];
        }
    }

    public function getRefreshToken($key, $secret, $code, $repo) {

        $this->consumerKey = $key;
        $this->consumerSecret = $secret;

        if (isset($this->refreshToken)) {
            echo "Refresh token is already set, remove the old line from the config.ini first!";
            return 2;
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bitbucket.org/site/oauth2/access_token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=authorization_code&code=".$code,
            CURLOPT_USERPWD => "$this->consumerKey:$this->consumerSecret",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = json_decode(curl_exec($curl), TRUE);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return $err;
        } else {
            if (isset($response['refresh_token'])) {
                file_put_contents($this->configFile, "refresh_token = " . $response['refresh_token'].PHP_EOL, FILE_APPEND);
                file_put_contents($this->configFile, "consumer_key = " . $key.PHP_EOL, FILE_APPEND);
                file_put_contents($this->configFile, "consumer_secret = " . $secret.PHP_EOL, FILE_APPEND);
                file_put_contents($this->configFile, "repo = " . $repo.PHP_EOL, FILE_APPEND);
            }
            return 0;
        }
    }

    public function submitIssue($title, $content, $priority="trivial", $kind="enhancement") {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bitbucket.org/1.0/repositories/$this->repoSuffix/issues",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "title=".urlencode($title)."&content=".urlencode($content)."&priority=".
                $priority."&kind=".$kind."&access_token=".$this->apiKey,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return 0;
        }
    }

    public function deleteIssue($ticketID) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bitbucket.org/1.0/repositories/$this->repoSuffix/issues/".$ticketID."?access_token=".$this->apiKey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_POSTFIELDS => "access_token=".$this->apiKey,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return 0;
        }
    }

    public function getAllIssues() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bitbucket.org/1.0/repositories/$this->repoSuffix/issues?access_token=".$this->apiKey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public function getIssueComments($issueID) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bitbucket.org/1.0/repositories/$this->repoSuffix/issues/".$issueID."/comments?access_token=".$this->apiKey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public function addCommentToIssue($issueID, $content) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bitbucket.org/1.0/repositories/".$this->repoSuffix."/issues/".$issueID."/comments",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "content=".$content."&access_token=".$this->apiKey,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return 0;
        }
    }





}