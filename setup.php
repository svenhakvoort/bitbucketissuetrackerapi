<?php

require 'src/BitbucketIssueTrackerApi.php';

$tracker = new \BitBucketApi\BitbucketIssueTrackerApi();
$user = readline("Enter your account name: ");
$repo = $user."/".readline("Enter your repo name: ");
$key = readline("Enter the key of the consumer: ");
$secret = readline("Enter the secret key of the consumer: ");
$code = readline("Enter the code given by https://bitbucket.org/site/oauth2/authorize?client_id={client_id}&response_type=code: ");
$tracker->getRefreshToken($key, $secret, $code, $repo);